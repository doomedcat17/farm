package Farm.Food;

public class Milk implements Food {

    FoodType foodType = FoodType.MILK;
    @Override
    public FoodType getFoodtype() {
        return foodType;
    }
}
