package Farm;


import java.util.concurrent.TimeUnit;

public class Life implements Runnable {
    @Override
    public void run() {
        Owner owner = new Owner();
        owner.adopt("Cat", "Papesz");
        owner.adopt("Dog", "Saba");
        owner.adopt("Goose", "Wojtek");
        try {
            while (!owner.getListOfAnimals().isEmpty()){
                for (int i = 0; i < owner.getListOfAnimals().size(); i++) {
                    TimeUnit.MILLISECONDS.sleep(50);
                    if(owner.getListOfAnimals().get(i).hungerIncrease()){
                        owner.animalDeath(owner.getListOfAnimals().get(i).getName());
                    }
                }
                owner.checkAnimals();
            }
            System.out.println("All animals are dead!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
