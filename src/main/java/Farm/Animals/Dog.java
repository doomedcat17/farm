package Farm.Animals;

public class Dog implements Animal {

    private String name;
    private int hunger = 0;
    private final int HUNGER_LIMIT = 100;
    private boolean isDead = false;
    private boolean isHungry = false;
    @Override
    public void feed() {
        hunger = hunger-10;
        if (hunger == 0){
            speak();
            System.out.println(" "+name+" is full!");
            isHungry = false;
        }
    }
    public boolean isDead() {
        return isDead;
    }

    public boolean isHungry() {
        return isHungry;
    }

    @Override
    public boolean hungerIncrease() {
        if (hunger==HUNGER_LIMIT){
            System.out.println(name+" is dead! :<");
            isDead = true;
            return true;
        }
        if (hunger > 50){
            System.out.println(name+" is hungry!");
            isHungry = true;
        }
        hunger = hunger+10;
        return false;
    }
    @Override
    public void speak() {
        System.out.print("Hau!");
    }

    @Override
    public void introduce() {
        System.out.println("Hau! My name is "+name+". I am a dog!");
    }

    public Dog(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
