package Farm.Animals;


import Farm.Food.FoodType;

public class Cat implements Animal {

    private String name;
    private int hunger = 0;
    private final int HUNGER_LIMIT = 100;
    private boolean isDead = false;
    private boolean isHungry = false;
    private FoodType[] ListOfFoodTypes = {FoodType.MEAT, FoodType.MILK};

    public boolean isDead() {
        return isDead;
    }

    public boolean isHungry() {
        return isHungry;
    }

    @Override
    public boolean hungerIncrease() {
        if (hunger==HUNGER_LIMIT){
            System.out.println(name+" is dead! :<");
            isDead = true;
            return true;
        }
        if (hunger > 50){
            System.out.println(name+" is hungry!");
            isHungry = true;
        }
        hunger = hunger+10;
        return false;
    }

    @Override
    public void feed() {
        hunger = hunger-10;
        if (hunger == 0){
            speak();
            System.out.println(" "+name+" is full!");
            isHungry = false;
        }
    }

    @Override
    public void speak() {
        System.out.print("Meeow!");
    }

    @Override
    public void introduce() {
        System.out.println("Meow! My name is "+name+". I am a cat!");
    }


    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

