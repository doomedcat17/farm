package Farm.Food;

public class Meat implements Food {

    FoodType foodType = FoodType.MEAT;
    @Override
    public FoodType getFoodtype() {
        return foodType;
    }
}
