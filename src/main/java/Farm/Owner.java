package Farm;

import Farm.Animals.Animal;
import Farm.Animals.Cat;
import Farm.Animals.Dog;
import Farm.Animals.Goose;

import java.util.ArrayList;

public class Owner {

    private ArrayList<Animal> listOfAnimals = new ArrayList<>();

    public void adopt(String animalType, String name){
        Animal animal = null;
        if (animalType.equals("Cat")){
            animal = new Cat(name);
        }
        if (animalType.equals("Dog")){
            animal = new Dog(name);
        }
        if (animalType.equals("Goose")){
            animal = new Goose(name);
        }
        listOfAnimals.add(animal);
        animal.introduce();
        animal.speak();
    }

    public ArrayList<Animal> getListOfAnimals() {
        return listOfAnimals;
    }
    public void animalDeath(String name){
        ArrayList<Animal> animals = listOfAnimals;
        for (int i = 0; i < animals.size() ; i++) {
            if (animals.get(i).getName().equals(name)) {
                animals.remove(i);
                break;
            }
        }
        listOfAnimals = animals;
    }
    public void checkAnimals(){
        for (int i = 0; i < listOfAnimals.size() ; i++) {
            if (listOfAnimals.get(i).isHungry()){
                while (listOfAnimals.get(i).isHungry()){
                    listOfAnimals.get(i).feed();
                }
            }
        }
    }
}
