package Farm.Animals;


public interface Animal {

    void feed();

    void speak();

    void introduce();

    boolean hungerIncrease();

    String getName();

    boolean isHungry();

}
